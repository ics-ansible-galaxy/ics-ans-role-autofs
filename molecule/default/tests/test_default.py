import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_autofs_running_and_enables(host):
    service = host.service("autofs")
    assert service.is_enabled
    assert service.is_running


def test_autofs_configuration(host):
    assert host.file("/etc/auto.master").contains("/mnt/nfs     /etc/autofs.nfs")
    assert host.file("/etc/autofs.nfs").exists
